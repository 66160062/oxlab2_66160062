

package com.mycompany.ox2;
import java.util.Scanner;
public class OX2 {
    public static char[] table ={' ',' ',' ',' ',' ',' ',' ',' ',' ',};
    private static int space = 0;
    private static boolean x_start = true;    
    public static void main(String[] args) {
       WelcomeXO();
       ExampleDisplay();
       Display();
       while(CheckWin()[0]){
           ShowTurn(x_start);
           UserInput();
           Display();
           x_start=!x_start;
       }
       if(CheckWin()[1]){
           System.out.println("Draw\n");
       }else if(!x_start){
           System.out.println("X Win !\n");
       }else{
           System.out.println("O Win !\n");
       }
       Display();
    }
    public static void WelcomeXO(){
            System.out.print("Welcome to XO game\n");
    }
    public static void ExampleDisplay(){
            System.out.println(" 1 | 2 | 3 ");
            System.out.println("-----------");
            System.out.println(" 4 | 5 | 6 ");
            System.out.println("-----------");
            System.out.println(" 7 | 8 | 9 ");
            System.out.println("\n-- Start Game --\n");
    }
    public static void Display(){
            System.out.println(" "+table[0]+" | "+table[1]+" | "+table[2]+" ");
            System.out.println("-----------");
            System.out.println(" "+table[3]+" | "+table[4]+" | "+table[5]+" ");
            System.out.println("-----------");
            System.out.println(" "+table[6]+" | "+table[7]+" | "+table[8]+" \n");
            
    }
    public static void ShowTurn(boolean x_start){
        if(x_start){
            System.out.println("X Turn");           
        }else{
            System.out.println("O Turn");
        }
    }
    public static boolean CheckInt(int x){
        int[] board_range={1,2,3,4,5,6,7,8,9};
        for(int i = 0 ; i<9 ; i++){
            if(x==board_range[i]){
                return true;
            } 
        }
        return false;
    }
    public static void UserInput(){
        Scanner sc = new Scanner(System.in);
        try{
            String Input = sc.next();
            int x = Integer.parseInt(Input);
            if(CheckInt(x)){
                if(table[x-1]==' '){
                    if(x_start){
                        table[x-1]='X';
                    }else{
                        table[x-1]='O';
                    }space+=1;
                }else{
                    System.out.println("Bro it fill man try again");
                }
            }else {
                System.out.println("Please input number between 1 - 9 :");
            }
        }catch (Exception e){
            System.out.println("Try again Homie");
        }
    }
    public static boolean[] CheckWin(){
        boolean[]ans = new boolean[2];
        if(table[0]==table[1]&&table[0]==table[2]&&table[0]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[3]==table[4]&&table[3]==table[5]&&table[3]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[6]==table[7]&&table[6]==table[8]&&table[6]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[0]==table[3]&&table[0]==table[6]&&table[0]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[1]==table[4]&&table[1]==table[7]&&table[1]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }if(table[2]==table[5]&&table[2]==table[8]&&table[2]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[0]==table[4]&&table[0]==table[8]&&table[0]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(table[2]==table[4]&&table[2]==table[6]&&table[2]!=' '){
            ans[0]= false;
            ans[1]= false;
            return ans;
        }
        if(space==9){
            ans[0]= false;
            ans[1]= true;
            return ans;
        }
        ans[0]=true;
        ans[1]=false;
        return ans;
        
    }
}
